package com.test.nycschools

import android.app.Application
import com.test.nycschools.di.AppComponent
import com.test.nycschools.di.DaggerAppComponent

class NycApplication : Application() {
    lateinit var daggerApplicationComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        daggerApplicationComponent = DaggerAppComponent.builder()
            .build()
    }
    fun getAppComponent() = daggerApplicationComponent
}