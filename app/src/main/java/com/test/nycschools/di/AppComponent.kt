package com.test.nycschools.di

import com.test.nycschools.viewmodel.ViewModelFactory
import com.test.nycschools.di.module.RetroModule
import com.test.nycschools.viewmodel.NycSchoolDetailsViewModel
import com.test.nycschools.viewmodel.NycSchoolViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [RetroModule::class])
interface AppComponent {
    fun inject(githubRepoViewModel: NycSchoolViewModel)

    fun inject(githubRepoViewModel: NycSchoolDetailsViewModel)

    fun inject(factory: ViewModelFactory)
}
