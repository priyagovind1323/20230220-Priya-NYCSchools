package com.test.nycschools.di.module

import com.test.nycschools.service.ApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
object RetroModule {
    const val BASE_URL = "https://data.cityofnewyork.us/"

    @Singleton
    @Provides
    fun getRetrofitInstance() =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Singleton
    @Provides
    fun getRetrofitService(retrofit: Retrofit) =
        retrofit.create(ApiService::class.java)
}