package com.test.nycschools.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.test.nycschools.viewmodel.ViewModelFactory
import com.test.nycschools.R
import com.test.nycschools.model.SchoolInfo
import com.test.nycschools.viewmodel.NycSchoolDetailsViewModel
import kotlinx.android.synthetic.main.activity_nyc_school_detail.*

class NycSchoolDetailActivity : AppCompatActivity() {
    private lateinit var viewModel: NycSchoolDetailsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nyc_school_detail)
        val schoolInfo = intent.getSerializableExtra("data") as SchoolInfo

        // Displays School Information passed from School List screen

        schoolInfo.apply {
            txtSchoolName.text = school_name
            txtWebsite.text = getString(R.string.website, website)
            txtPhone.text = getString(R.string.phone, phone_number)
            txtEmail.text = getString(R.string.email, school_email)
            txtOverview.text = overview_paragraph
            txtOpportunities.text = academicopportunities1

            initViewModel(dbn)
        }
    }

    private fun initViewModel(dbn: String) {
        // Initialise View Model
        viewModel = ViewModelFactory(application).create(NycSchoolDetailsViewModel::class.java)

        // regsietering for LiveData Stats Records
        viewModel.registerForStats().observe(this, { stats ->
            progressBar.visibility = View.GONE
            stats?.let {
                // stats Received for specific dbn/school
                txtStats.text = getString(R.string.test_stats,
                    it.num_of_sat_test_takers,
                it.sat_critical_reading_avg_score,
                it.sat_math_avg_score,
                it.sat_writing_avg_score)
            } ?: kotlin.run { txtStats.text = getString(R.string.no_stats_available) }
        })
        progressBar.visibility = View.VISIBLE

        // fetching Stats
        viewModel.fetchStats(dbn)
    }
}