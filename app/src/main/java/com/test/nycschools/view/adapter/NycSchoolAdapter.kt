package com.test.nycschools.view.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.nycschools.R
import com.test.nycschools.model.SchoolInfo
import com.test.nycschools.view.NycSchoolDetailActivity
import kotlinx.android.synthetic.main.cell_school.view.*

class NycSchoolAdapter(var commitRecords: List<SchoolInfo>)
    : RecyclerView.Adapter<NycSchoolAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cell_school, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindRecord(commitRecords.get(position))
    }

    override fun getItemCount(): Int {
        return commitRecords.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindRecord(record: SchoolInfo) {
            record.apply {
                itemView.txtSchoolName.text = record.school_name
                itemView.txtOverview.text = record.overview_paragraph
                itemView.txtWebsite.text = record.website
            }
            itemView.setOnClickListener {
                val intent = Intent(itemView.context, NycSchoolDetailActivity::class.java).apply {
                    putExtra("data", record)
                }
                itemView.context.startActivity(intent)
            }
        }
    }
}