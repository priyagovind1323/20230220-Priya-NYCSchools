package com.test.nycschools.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.nycschools.viewmodel.ViewModelFactory
import com.test.nycschools.R
import com.test.nycschools.view.adapter.NycSchoolAdapter
import com.test.nycschools.viewmodel.NycSchoolViewModel
import kotlinx.android.synthetic.main.activity_main.*

class NycSchoolListActivity : AppCompatActivity() {
    lateinit var viewModel: NycSchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModel()
    }

    private fun initViewModel() {
        // initilialse view model
        viewModel = ViewModelFactory(application).create(NycSchoolViewModel::class.java)
        // observing livedata for schools information
        viewModel.registerForSchoolsInfo().observe(this, { schoolsList ->
            // callback received with list of school information
            showMessage(schoolsList.isNullOrEmpty())
            if (schoolsList.isNullOrEmpty()) {
                showMessage(true)
            } else {
                showMessage(false)
                recyclerView.apply {
                    layoutManager = LinearLayoutManager(this@NycSchoolListActivity)
                    addItemDecoration(
                        DividerItemDecoration(
                            this@NycSchoolListActivity,
                            DividerItemDecoration.VERTICAL
                        )
                    )
                    adapter = NycSchoolAdapter(schoolsList)
                }
            }
        })

        // Observing for network connection
        viewModel.registerForInternalConnectivity().observe(this, { isNetworkConnected ->
            if (!isNetworkConnected) {
                progressBar.visibility = View.GONE
                txtMessage.visibility = View.VISIBLE
                txtMessage.text = getString(R.string.no_network)
            }
        })
            progressBar.visibility = View.VISIBLE
        // fetch schools
        viewModel.fetchSchools()
    }

    private fun showMessage(show: Boolean) {
        progressBar.visibility = View.GONE
        recyclerView.visibility = if (show) View.GONE else View.VISIBLE
        txtMessage.visibility = if (show) View.VISIBLE else View.GONE
    }
}