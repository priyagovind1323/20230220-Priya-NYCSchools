package com.test.nycschools.viewmodel
import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.test.nycschools.NycApplication
import com.test.nycschools.service.ApiService
import com.test.nycschools.viewmodel.NycSchoolDetailsViewModel
import com.test.nycschools.viewmodel.NycSchoolViewModel
import javax.inject.Inject

class ViewModelFactory(
    private val application: Application
) :
    ViewModelProvider.Factory {

    init {
        (application as NycApplication).getAppComponent()
            .inject(this)
    }

    @Inject
    lateinit var apiService: ApiService

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NycSchoolViewModel::class.java)) {
            return NycSchoolViewModel(application, apiService) as T
        } else {
            return NycSchoolDetailsViewModel(application, apiService) as T
        }

    }
}