package com.test.nycschools.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.nycschools.NycApplication
import com.test.nycschools.Util
import com.test.nycschools.service.ApiService
import com.test.nycschools.model.SchoolInfo
import com.test.nycschools.model.StatsInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NycSchoolDetailsViewModel @Inject constructor(val application: Application, var retroApiService: ApiService) : ViewModel() {

    init {
        (application as NycApplication).getAppComponent()
            .inject(this)
    }

    // Mutable LiveData to hold Stats Information
    private var stats: MutableLiveData<StatsInfo> = MutableLiveData()

    fun registerForStats(): LiveData<StatsInfo> {
        return stats
    }

    fun fetchStats(dbn: String) {
        if (Util.isNetworkConnected(application)) {
            viewModelScope.launch {
                // retrofit call to fetch Stats
                val response = retroApiService.getStats()
                if (response.isSuccessful) {
                    // filtering out stats based on dbn
                    val info = response.body()?.firstOrNull {
                        it.dbn == dbn
                    }
                    // sending result to observer i.e Activity
                    stats.postValue(info)
                } else {
                    stats.postValue(null)
                }
            }
        } else {
            stats.postValue(null)
        }
    }
}