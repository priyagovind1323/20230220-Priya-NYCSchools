package com.test.nycschools.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.nycschools.NycApplication
import com.test.nycschools.Util
import com.test.nycschools.service.ApiService
import com.test.nycschools.model.SchoolInfo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class NycSchoolViewModel @Inject constructor(val application: Application, var retroApiService: ApiService) : ViewModel() {

    init {
        (application as NycApplication).getAppComponent()
            .inject(this)
    }

    private var schools: MutableLiveData<List<SchoolInfo>> = MutableLiveData()

    fun registerForSchoolsInfo(): LiveData<List<SchoolInfo>> {
        return schools
    }

    private var internetInfo: MutableLiveData<Boolean> = MutableLiveData()

    fun registerForInternalConnectivity(): LiveData<Boolean> {
        return internetInfo
    }

    fun fetchSchools() {
        if (Util.isNetworkConnected(application)) {
            viewModelScope.launch {
                val response = retroApiService.getSchoolInfo()
                if (response.isSuccessful) {
                    schools.postValue(response.body())
                } else {
                    schools.postValue(null)
                }
            }
        } else {
            internetInfo.postValue(false)
        }
    }
}